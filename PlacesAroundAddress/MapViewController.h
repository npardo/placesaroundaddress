//
//  MapViewController.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 10/31/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MapModel.h"



@interface MapViewController : UIViewController<MKMapViewDelegate, UITableViewDataSource, UITableViewDelegate>{
    IBOutlet MKMapView *map;
    MapModel *model;

    // Presents the types of locations
    IBOutlet UITableView *typeTV;
    IBOutlet UIView *typeTVHolder;
}

- (IBAction)chooseType:(id)sender;

@property (nonatomic, strong) NSString *coordinatesString;
@property (nonatomic, strong) NSString *type;
@end

//
//  LocatioManager.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

extern NSString *const LatKey;
extern NSString *const LngKey;
extern NSString *const CoordinateReadyNotification;
extern NSString *const CurrentLocationReadyNotification;

@interface LocatioManager : NSObject<CLLocationManagerDelegate>{
    CLLocationManager *locationManager;
}

+ (LocatioManager *)instance;

// The names of the methods describe the action
- (void)coordinatesForAddress:(NSString *)address;
- (CLLocationCoordinate2D)stringToCoordinate:(NSString *)coordinateString;
- (void)startTrackingCurrentLocation;

@property (nonatomic, copy) NSDictionary *coordinates;
@property (nonatomic, assign) CLLocationCoordinate2D currentLocation;

@end

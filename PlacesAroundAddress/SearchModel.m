//
//  SearchModel.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 10/31/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "SearchModel.h"

NSString *const ResultsReadyNotification = @"ResultsReadyNotification";
NSString *const DescriptionKey = @"description";

static NSString *const getUrl = @"https://maps.googleapis.com/maps/api/place/autocomplete/json?input=%@&types=geocode&language=en&sensor=true&key=%@";

#define API_KEY @"AIzaSyACWWtCCb96Vg6n09cx87rMzHQge_bxBoI"
#define RESULTS @"predictions"

@implementation SearchModel


#pragma mark
#pragma mark Private methods

// Trigger the get request from google
- (void)resultsFromGoogleAtUrl:(NSURL *)link{
    DownloadAccessor *downloader = [DownloadAccessor new];
    [downloader startDownloadAtUrl:link
               withCompletionBlock:^(NSData *data, long long dataSize, long long totalSize, NSError *error) {
                   if (data) {
                       [self prepareResults:data];
                   } else if (error){
                       NSLog(@"Error Downloading file");
                   }
               }];
}

// Parse the json data and post it to the searchViewController
- (void)prepareResults:(NSData *)jsonData{
    NSError *parsedError = nil;
    NSDictionary *parsedFile = [NSJSONSerialization JSONObjectWithData:jsonData
                                                               options:kNilOptions
                                                                 error:&parsedError];
    _results = parsedFile[RESULTS];
    if (!cachedResults) {
        cachedResults = [NSMutableDictionary new];
    }
    // store it for reuse when the user will type the same string
    cachedResults[textKey] = _results;
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:ResultsReadyNotification
                                                            object:nil];
    });
}

#pragma mark 
#pragma mark Public methods
- (NSArray *)typed:(NSString *)text{
    BOOL contain = NO;
    textKey = text;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF IN %@", cachedResults.allKeys];
    contain = [predicate evaluateWithObject:text];
    
    if (contain) {
        return cachedResults[text];
    }else{
        NSString *strURL = [NSString stringWithFormat:getUrl, text, API_KEY];
        [self resultsFromGoogleAtUrl:[NSURL URLWithString:strURL]];
    }
    return nil;
}

- (void)cleanCache{
    cachedResults = nil;
}

- (CGRect)tableViewRect:(NSNotification *)keyboardNotification tvRect:(CGRect)tvRect isShow:(int)isShow{
    NSValue *keyboardVal = keyboardNotification.userInfo[UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [keyboardVal CGRectValue];
    return CGRectMake(tvRect.origin.x,
                      tvRect.origin.y,
                      tvRect.size.width,
                      tvRect.size.height + isShow * keyboardRect.size.height);
}
@end

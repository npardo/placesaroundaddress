//
//  MapModel.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "MapModel.h"
#import "DataBaseManager.h"
#import "OIPImageAccessor.h"


#define SCROLL_UPDATE_DISTANCE 5

static NSString *linkFormat = @"https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=%@&radius=%i&types=%@&name=%@&sensor=false&key=%@";

NSString *const AnnotationDBReadyNotification = @"AnnotationDBReadyNotification";
#define GOOGLE_API_KEY @"AIzaSyACWWtCCb96Vg6n09cx87rMzHQge_bxBoI"
@implementation MapModel
@synthesize types = _types;


- (id)init{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateAnnotation)
                                                     name:AnnotationsReadyNotification
                                                   object:nil];
        return self;
    }
    return nil;
}

- (void)updateAnnotation{
    [[NSNotificationCenter defaultCenter] postNotificationName:AnnotationDBReadyNotification
                                                        object:nil];
}

// Call the request from google according to all parameters
- (void)MOContextReady{
    NSString *currentCenter = [NSString stringWithFormat:@"%f,%f", currentCentre.latitude, currentCentre.longitude];
    NSString *getRequest = [NSString stringWithFormat:linkFormat, currentCenter, currenDist,_type, @"", GOOGLE_API_KEY];
    [DataBaseManager instance].annotationType = _type;
    [[DataBaseManager instance] updateDataBase:[NSURL URLWithString:getRequest]];
}


// Gets the coordinates as a string: can be from the search result or from the current location
- (CLLocationCoordinate2D)mapCenter:(NSString *)centerString type:(NSString *)lType{
    currentCoordinate = centerString;
    _type = lType;
    firstCenter = [[LocatioManager instance] stringToCoordinate:centerString];
    [self MOContextReady];
    return firstCenter;
}



// Lazy download of the image which in the json file
- (void)getAnnotationIcon:(AnnotationObj *)annotaion into:(id)container{
    OIPImageAccessor *imgAccessor = [OIPImageAccessor new];
    [imgAccessor flagImageAtUrl:annotaion.imageURL
                       withName:annotaion.imageURL
                    atImageView:(UIImageView *)container];
}

- (MKCoordinateRegion)currentRegion:(MKMapView *)mapView
{
    
    //Zoom back to the user location after adding a new set of annotations.
    
    //Get the center point of the visible map.
    CLLocationCoordinate2D centre = [mapView centerCoordinate];
    
    MKCoordinateRegion region;
    
    //Set the center point to the visible region of the map and change the radius to match the search radius passed to the Google query string.
    region = MKCoordinateRegionMakeWithDistance(centre,currenDist,currenDist);
    
    
    
    return region;
}


// Update the distance for getting the correct get requeat from google
- (void)updateRadius:(MKMapView *)inMap{
    //Get the east and west points on the map so we calculate the distance (zoom level) of the current map view.
    MKMapRect mRect = inMap.visibleMapRect;
    
    // width
    MKMapPoint eastMapPoint = MKMapPointMake(MKMapRectGetMinX(mRect), MKMapRectGetMidY(mRect));
    MKMapPoint westMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), MKMapRectGetMidY(mRect));
    
    //Set our current distance instance variable.
    currenDist = MKMetersBetweenMapPoints(eastMapPoint, westMapPoint);
    
    //Set our current centre point on the map instance variable.
    currentCentre = inMap.centerCoordinate;
}

- (NSArray *)storedAnnotations{
    [DataBaseManager instance].annotationType = _type;
    return [[DataBaseManager instance] annotationsFromDB];
}

- (NSArray *)itemsByType:(NSString *)annType{
    return [[DataBaseManager instance] itemByEntity:@"Annotaion"
                                       andAttribute:@"type"
                                             andVal:annType];
}

- (BOOL)isExistAnnotation:(NSString *)uniqueId{
    [DataBaseManager instance].annotationType = uniqueId;
    NSArray *matches = [[DataBaseManager instance] annotationsFromDB];
    // instead of predicate (I didn't had the time to investigate why it didn't work)
    if (matches.count){
        NSLog(@"OK");
    }
    return matches.count;
}

// Get the list of types from the plist
- (NSDictionary *)types{
    if (!_types) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Types" ofType:@"plist"];
        _types = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    return _types;
}

- (void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    currentCoordinate = nil;
    _type = nil;
}
@end

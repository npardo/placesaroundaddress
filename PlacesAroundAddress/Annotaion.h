//
//  Annotaion.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/3/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Annotaion : NSManagedObject

@property (nonatomic, retain) NSString * icon;
@property (nonatomic, retain) NSNumber * lat;
@property (nonatomic, retain) NSNumber * longt;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSData * photo;
@property (nonatomic, retain) NSNumber * rating;
@property (nonatomic, retain) NSString * reference;
@property (nonatomic, retain) NSData * types;
@property (nonatomic, retain) NSString * uniqueID;
@property (nonatomic, retain) NSString * vicinity;
@property (nonatomic, retain) NSString * type;

@end

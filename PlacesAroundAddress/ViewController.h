//
//  ViewController.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 10/31/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "SearchViewController.h"
#import "LobbyModel.h"


@interface ViewController : UIViewController<SearchDelegate, UITableViewDataSource, UITableViewDelegate>{
    LobbyModel *model;
    
    // Choose between Current location to any address
    IBOutlet UISegmentedControl *control;
    
    // Will present the current location
    IBOutlet UILabel *currentLocationAddress;
    
    // Will present the types of Locations
    IBOutlet UITableView *typesTV;
    
    IBOutlet UILabel *chooseType;
    
    NSString *locType;
    
    // Stores the coordinate for sending to the map controller
    NSString *coordinateForMap;
}

// Choose between currnent location and some address
- (IBAction)addressType:(UISegmentedControl *)sender;

@end

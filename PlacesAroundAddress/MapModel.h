//
//  MapModel.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "LocatioManager.h"
#import "AnnotationObj.h"

extern NSString *const AnnotationDBReadyNotification;

@interface MapModel : NSObject{
    NSString *currentCoordinate;
    CLLocationCoordinate2D currentCentre;
    CLLocationCoordinate2D firstCenter;
    int currenDist;
    BOOL firstLaunch;
}


/*  Get the coordinate string from the lobby and transform it to CLLocationCoordinate2D
    can be the current location, or a search result.
 */
- (CLLocationCoordinate2D)mapCenter:(NSString *)centerString type:(NSString *)lType;


- (void)MOContextReady;

- (void)updateRadius:(MKMapView *)inMap;

- (MKCoordinateRegion)currentRegion:(MKMapView *)inMap;

// Calls the icon image for the annotation (google link in the json)
- (void)getAnnotationIcon:(AnnotationObj *)annotaion into:(id)container;

// Will store all the annotations data from the data base each time the user will change the choice
- (NSArray *)storedAnnotations;

// Checks existece of the annotaion in the data base
- (BOOL)isExistAnnotation:(NSString *)uniqueId;

// For the types list
@property (nonatomic, strong, readonly) NSDictionary *types;

// Stores the type (from the map controller, by the user choice)
@property (nonatomic, strong) NSString *type;
@end

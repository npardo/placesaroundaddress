//
//  OIPImageAccessor.h
//  OddsInPlayNew
//
//  Created by Nissim Pardo on 7/5/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface OIPImageAccessor : NSObject 

- (void)flagImageAtUrl:(NSString *)urlString
              withName:(NSString *)name
           atImageView:(UIImageView *)imgView;


@end

//
//  DownloadAccessor.m
//  SportsTrackerCD
//
//  Created by Nissim Pardo on 9/28/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "DownloadAccessor.h"
@interface DownloadAccessor()<NSURLConnectionDataDelegate>{
    downloadBlock _downloadBlock;
    NSMutableData *appendData;
}
@end
@implementation DownloadAccessor

- (void)startDownloadAtUrl:(NSURL *)url
       withCompletionBlock:(downloadBlock)block{
    _downloadBlock = [block copy];
    appendData = [NSMutableData new];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request
                                                                  delegate:self
                                                          startImmediately:YES];
    [connection start];
}

#pragma mark 
#pragma mark NSUrlConnectionDelegate methods
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [appendData appendData:data];
    _downloadBlock(nil, data.length, -1, nil);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    _downloadBlock([appendData copy], -1, -1, nil);
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    _downloadBlock(nil, -1, -1, error);
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSHTTPURLResponse *)response{
    
    _downloadBlock(nil, -1, [response.allHeaderFields[@"Content-Size"] integerValue], nil);
}

- (void)dealloc{
    _downloadBlock = nil;
    appendData = nil;
}
@end

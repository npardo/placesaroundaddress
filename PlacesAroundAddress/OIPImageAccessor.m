//
//  OIPImageAccessor.m
//  OddsInPlayNew
//
//  Created by Nissim Pardo on 7/5/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "OIPImageAccessor.h"


@implementation OIPImageAccessor



- (void)flagImageAtUrl:(NSString *)urlString
              withName:(NSString *)name
           atImageView:(UIImageView *)imgView {
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingFormat:@"%@", name];
    UIImage *image = nil;
    if ([[NSFileManager defaultManager] fileExistsAtPath:fullPath]) {
        image = [self cachedImage:fullPath];
        if (image) {
            imgView.image = image;
        }else{
            NSLog(@"Image Not Valid");
        }
    }else{
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlString]];
        [NSURLConnection sendAsynchronousRequest:request
                                           queue:[NSOperationQueue mainQueue]
                               completionHandler:^(NSURLResponse *response, NSData *imgData, NSError *error) {
                                   UIImage *img = [UIImage imageWithData:imgData];
                                   if (img) {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           imgView.image = [self resizeImage:img
                                                                      toSize:CGSizeMake(10, 10)];
                                       });
                                       [self storeImageData:imgData withName:name];
                                   }else{
                                       NSLog(@"Image not valid");
                                   }
                               }];
    }
    
}

- (UIImage *)cachedImage:(NSString *)path {
    NSData *imgData = [NSData dataWithContentsOfFile:path];
    UIImage *image = [UIImage imageWithData:imgData];
    if (image != nil) {
        return image;
    }else{
        NSLog(@"WHImageAccessor - image not valid");
    }
    return nil;
}

// Stores the downloaded image in the file system
- (void)storeImageData:(NSData *)data withName:(NSString *)name {
    NSString *fullPath = [NSTemporaryDirectory() stringByAppendingFormat:@"%@", name];
    if ([data writeToFile:fullPath atomically:YES]) {
        NSLog(@"IMAGE SAVED");
    }else{
        NSLog(@"IMAGE NOT SAVED");
    }
}

- (UIImage *)resizeImage:(UIImage *)img toSize:(CGSize)size{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0.0);
    [img drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end

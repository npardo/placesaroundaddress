//
//  AnnotationObj.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "AnnotationObj.h"

@implementation AnnotationObj

- (id)initWithAnnotation:(Annotaion *)annotation{
    self = [super init];
    if (self) {
        curAnnotation = annotation;
        return self;
    }
    return nil;
}
- (CLLocationCoordinate2D)coordinate{
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [curAnnotation.lat floatValue];
    coordinate.longitude = [curAnnotation.longt floatValue];
    return coordinate;
}

- (NSString *)title{
    return curAnnotation.name;
}

- (NSString *)subtitle{
    return nil;
}

- (NSString *)imageURL{
    return curAnnotation.icon;
}

- (NSString *)uniqueID{
    return curAnnotation.uniqueID;
}
- (void)setCoordinate:(CLLocationCoordinate2D)newCoordinate{
    
}

@end

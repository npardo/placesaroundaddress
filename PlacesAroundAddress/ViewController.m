//
//  ViewController.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 10/31/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "ViewController.h"
#import "MapViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    model = [LobbyModel new];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reversedLocationReady:)
                                                 name:CoordinateReadyNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(currentLocationReady:)
                                                 name:CurrentLocationReadyNotification
                                               object:nil];
    [model getCurrentLocation];
    currentLocationAddress.layer.cornerRadius = 5;
    chooseType.layer.cornerRadius = 3;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



// Update the UI according to the user selection
- (IBAction)addressType:(UISegmentedControl *)sender{
    currentLocationAddress.text = @"";
    model.isCurrent = sender.selectedSegmentIndex;
    if (sender.selectedSegmentIndex == 0) {
        typesTV.hidden = YES;
        chooseType.hidden = YES;
        [model getCurrentLocation];
    }else{
        [self performSegueWithIdentifier:@"SearchScreen"
                                  sender:self];
    }
}

// Sends to the map controller all the parameters for rendring the annotations and the address
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(NSString *)sender{
    if ([segue.identifier isEqualToString:@"SearchScreen"]) {
        [segue.destinationViewController setDelegate:self];
    }else if ([segue.identifier isEqualToString:@"MapScreen"]){
        [segue.destinationViewController setCoordinatesString:coordinateForMap];
        [segue.destinationViewController setType:locType];
    }
}

// Call back for the address (reverse geocoding)
- (void)currentLocationReady:(NSNotification *)notification{
    if (notification.object) {
        currentLocationAddress.text = [model addressFromPlaceMark:notification.object];
    }else{
        coordinateForMap = model.currentLocation;
        typesTV.hidden = NO;
        chooseType.hidden = NO;
    }
}

// Call back with the coordinate bt using reverse geocoding
- (void)reversedLocationReady:(NSNotification *)notification{
    typesTV.hidden = NO;
    chooseType.hidden = NO;
    coordinateForMap = notification.object;
}

#pragma mark SearchDelegate methods
// Get the result of the picked address
- (void)searchVC:(SearchViewController *)controller didSelectResult:(NSDictionary *)result{
    currentLocationAddress.text = result[DescriptionKey];
    [model coordinatesForAddress:result];
}

- (void)searchCancelled{
    currentLocationAddress.text = @"";
}

#pragma mark UITableViewDelegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return model.types.allKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"TypeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:Identifier];
    }
    cell.textLabel.text = model.types.allKeys[indexPath.row];
    cell.textLabel.textColor = [UIColor colorWithRed:(3/255.f) green:(122/255.f) blue:(255/255.f) alpha:1.0];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    locType = model.types.allValues[indexPath.row];
    [self performSegueWithIdentifier:@"MapScreen" sender:nil];
}
@end

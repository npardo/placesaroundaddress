//
//  MapViewController.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 10/31/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "MapViewController.h"
#import "AnnotationObj.h"




#define MAP_WIDTH 1000
#define MAP_HEIGHT 1000



@interface MapViewController ()

@end

@implementation MapViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addAnnotationFromDB)
                                                 name:AnnotationDBReadyNotification
                                               object:nil];
    model = [[MapModel alloc] init];
    [self setRegion];
}

- (void)viewWillDisappear:(BOOL)animated{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    model = nil;
    [super viewWillDisappear:animated];
}

- (IBAction)chooseType:(id)sender{
    typeTV.layer.cornerRadius = 3;
    [UIView animateWithDuration:0.3
                     animations:^{
                         typeTVHolder.alpha = 1;
                     }];
}

// Sets the map for the first time
- (void)setRegion{
    CLLocationCoordinate2D center = [model mapCenter:_coordinatesString type:_type];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(center, MAP_WIDTH, MAP_HEIGHT);
    [map setRegion:region animated:YES];
    [self addAnnotationFromDB];
}

// Remove all the annotations (just when the user choose different type)
- (void)removeAnnotations{
    for (AnnotationObj *annotation in map.annotations) {
        [map removeAnnotation:annotation];
    }
}

// Checks the data base for existence and add annotations which not exist (for better performance)
- (void)addAnnotationFromDB{
    for (Annotaion *ann in [model storedAnnotations]) {
        if (![model isExistAnnotation:ann.uniqueID]){
            AnnotationObj *obj = [[AnnotationObj alloc] initWithAnnotation:ann];
            [map addAnnotation:obj];
        }
    }
}




#pragma mark MKMapViewDelegate methods
- (MKAnnotationView *)mapView:(MKMapView *)mapView
            viewForAnnotation:(id<MKAnnotation>)annotation{
    static NSString *Identifier = @"PlaceAnnotation";
    if ([annotation isKindOfClass:[AnnotationObj class]]) {
        MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:Identifier];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation
                                                          reuseIdentifier:Identifier];
        }
        annotationView.enabled = YES;
        annotationView.canShowCallout = YES;
        
        [model getAnnotationIcon:annotation into:annotationView];
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views{
    [model currentRegion:mapView];
}



- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    // Updates the map dynamically
    [model updateRadius:mapView];
    [model MOContextReady];
}


#pragma mark UITableViewDelegate methods
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return model.types.allKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"TypeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:Identifier];
    }
    cell.textLabel.text = model.types.allKeys[indexPath.row];
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:12.0]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [UIView animateWithDuration:0.3
                     animations:^{
                         typeTVHolder.alpha = 0;
                     }];
    // Change the type of annotations
    model.type = model.types.allValues[indexPath.row];
    [self removeAnnotations];
    [model MOContextReady];
}
@end

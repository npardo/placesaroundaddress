//
//  SearchViewController.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 10/31/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resultsReady)
                                                 name:ResultsReadyNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardShow:)
                                                 name:UIKeyboardDidShowNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    model = [SearchModel new];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Get the notification for new addresses and update the list
- (void)resultsReady{
    results = model.results;
    [resultsTV reloadData];
}

// Changes the size of the tableview according to the keyboard state
- (void)keyboardShow:(NSNotification *)notification{
    resultsTV.frame = [model tableViewRect:notification
                                tvRect:resultsTV.frame
                                isShow:-1];
}

// Changes the size of the tableview according to the keyboard state
- (void)keyboardHide:(NSNotification *)notification{
    resultsTV.frame = [model tableViewRect:notification
                                tvRect:resultsTV.frame
                                isShow:1];
}

#pragma mark UITableViewDelegate methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return results.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *Identifier = @"SearchResult";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:Identifier];
    }
    cell.textLabel.text = results[indexPath.row][DescriptionKey];
    cell.textLabel.textColor = [UIColor colorWithRed:(3/255.f) green:(122/255.f) blue:(255/255.f) alpha:1.0];
    [cell.textLabel setFont:[UIFont fontWithName:@"Helvetica" size:12.0]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([_delegate respondsToSelector:@selector(searchVC:didSelectResult:)]) {
        NSLog(@"%d", indexPath.row);
        [_delegate searchVC:self didSelectResult:results[indexPath.row]];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
    [model cleanCache];
}

#pragma mark UISearchBarDelegate methods
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    searchText = [searchText stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    results = [model typed:searchText];
    if (results && results.count) {
        [resultsTV reloadData];
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];
    if ([_delegate respondsToSelector:@selector(searchCancelled)]) {
        [_delegate searchCancelled];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
}
@end

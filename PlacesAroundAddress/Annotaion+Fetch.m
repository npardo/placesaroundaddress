//
//  Annotaion+Fetch.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "Annotaion+Fetch.h"

NSString *const LatKeyPath = @"geometry.location.lat";
NSString *const LongKeyPath = @"geometry.location.lng";
NSString *const IconKey = @"icon";
NSString *const IDKey = @"id";
NSString *const NameKey = @"name";
NSString *const PhotosKey = @"photos";
NSString *const RatingKey = @"rating";
NSString *const TypesKey = @"types";
NSString *const VicinityKey = @"vicinity";

@implementation Annotaion (Fetch)
+ (Annotaion *)annotationWithDictionary:(NSDictionary *)params
                        inManagedObject:(NSManagedObjectContext *)context
                               withType:(NSString *)type{
    Annotaion *annotation = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Annotaion"];
    request.predicate = [NSPredicate predicateWithFormat:@"uniqueID == %@", params[IDKey]];
    
    NSError *error = nil;
    NSArray *matches = [context executeFetchRequest:request error:&error];
    if (!matches || matches.count > 1) {
        NSLog(@"%@", [error description]);
    }else if (!matches.count){
        annotation = [NSEntityDescription insertNewObjectForEntityForName:@"Annotaion"
                                              inManagedObjectContext:context];
        annotation.uniqueID = params[IDKey];
        annotation.icon = params[IconKey];
        annotation.lat = [params valueForKeyPath:LatKeyPath];
        annotation.longt = [params valueForKeyPath:LongKeyPath];
        annotation.name = params[NameKey];
        annotation.photo = [NSKeyedArchiver archivedDataWithRootObject:params[PhotosKey]];
        annotation.rating = params[RatingKey];
        annotation.types = [NSKeyedArchiver archivedDataWithRootObject:params[TypesKey]];
        annotation.type = type;
        annotation.vicinity = params[VicinityKey];
    }else{
        annotation = [matches lastObject];
    }
    return annotation;
}
@end

//
//  SearchViewController.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 10/31/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchModel.h"
@class SearchViewController;

@protocol SearchDelegate <NSObject>

- (void)searchVC:(SearchViewController *)controller didSelectResult:(NSDictionary *)result;
- (void)searchCancelled;

@end


@interface SearchViewController : UIViewController<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate>{
    
    // Presents the dynamic addresses from google
    IBOutlet UITableView *resultsTV;
    IBOutlet UISearchBar *mySearchBar;
    SearchModel *model;
    NSArray *results;
}

@property (nonatomic, weak) id<SearchDelegate> delegate;
@end

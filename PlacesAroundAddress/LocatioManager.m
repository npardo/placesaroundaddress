//
//  LocatioManager.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "LocatioManager.h"

NSString *const CoordinateReadyNotification = @"CoordinateReadyNotification";
NSString *const CurrentLocationReadyNotification = @"CurrentLocationReadyNotification";

NSString *const LatKey = @"LatKey";
NSString *const LngKey = @"LngKey";


@implementation LocatioManager

+ (LocatioManager *)instance{
    static LocatioManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)startTrackingCurrentLocation{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locationManager startUpdatingLocation];
}


- (void)coordinatesForAddress:(NSString *)address{
    CLGeocoder *geocoder = [CLGeocoder new];
    [geocoder geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
        if ([placemarks count] > 0) {
            CLPlacemark *placemark = placemarks[0];
            CLLocation *location = placemark.location;
            CLLocationCoordinate2D coordinate = location.coordinate;
            NSString *coordinates = [NSString stringWithFormat:@"%f,%f", coordinate.latitude, coordinate.longitude];
            [[NSNotificationCenter defaultCenter] postNotificationName:CoordinateReadyNotification
                                                                object:coordinates];
        }
    }];
}

- (void)getCurrentLocationAddress:(CLLocation *)location{
    CLGeocoder *geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location
                   completionHandler:^(NSArray *placemarks, NSError *error) {
                       if (placemarks.count) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               [[NSNotificationCenter defaultCenter] postNotificationName:CurrentLocationReadyNotification object:placemarks[0]];
                           });
                           locationManager.delegate = nil;
                       }
                   }];
}

- (CLLocationCoordinate2D)stringToCoordinate:(NSString *)coordinateString{
    CLLocationCoordinate2D location = kCLLocationCoordinate2DInvalid;
    NSArray *coordinates = [coordinateString componentsSeparatedByString:@","];
    if (coordinates.count == 2) {
        location.latitude = [coordinates[0] floatValue];
        location.longitude = [coordinates[1] floatValue];
    }
    return location;
}

#pragma mark CLLocationManagerDelegate methods
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
    CLLocation *currentLocation = [locations lastObject];
    if (currentLocation != nil) {
        _currentLocation.latitude = currentLocation.coordinate.latitude;
        _currentLocation.longitude = currentLocation.coordinate.longitude;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:CurrentLocationReadyNotification
                                                                object:nil];
        });
        [self getCurrentLocationAddress:currentLocation];
        locationManager.delegate = nil;
        [manager stopUpdatingLocation];
    }
}

@end

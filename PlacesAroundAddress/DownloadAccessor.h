//
//  DownloadAccessor.h
//  SportsTrackerCD
//
//  Created by Nissim Pardo on 9/28/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^downloadBlock) (NSData *data, long long dataSize, long long totalSize, NSError *error);

@interface DownloadAccessor : NSObject

- (void)startDownloadAtUrl:(NSURL *)url
       withCompletionBlock:(downloadBlock)block;
@end

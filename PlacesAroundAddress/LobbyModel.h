//
//  LobbyModel.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LocatioManager.h"


@interface LobbyModel : NSObject{
    NSString *reversedLocation;
}
// Gets dictionary from the search screen and returns a string
- (void)coordinatesForAddress:(NSDictionary *)address;

// Trigger the action for finding the current location (returns by notification)
- (void)getCurrentLocation;

// Transform placemark to 1 string with all the address
- (NSString *)addressFromPlaceMark:(CLPlacemark *)placemark;

// Contains the list of types, which Google supports (plist)
@property (nonatomic, strong, readonly) NSDictionary *types;

// Stores the current location
@property (nonatomic, strong) NSString *currentLocation;

// Stores the user choice between current location to any address
@property (nonatomic, assign) BOOL isCurrent;

// Stores the updated cooredinates in string
@property (nonatomic, strong, readonly) NSString *coordinateForMap;
@end

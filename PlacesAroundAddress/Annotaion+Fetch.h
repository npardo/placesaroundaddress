//
//  Annotaion+Fetch.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "Annotaion.h"

extern NSString *const LatKeyPath;
extern NSString *const LongKeyPath;
extern NSString *const IconKey;
extern NSString *const IDKey;
extern NSString *const NameKey;
extern NSString *const PhotosKey;
extern NSString *const RatingKey;
extern NSString *const TypesKey;
extern NSString *const VicinityKey;

@interface Annotaion (Fetch)
// Adds once time the annotation according to the id of Google
+ (Annotaion *)annotationWithDictionary:(NSDictionary *)params
                        inManagedObject:(NSManagedObjectContext *)context
                               withType:(NSString *)type;
@end

//
//  Annotaion.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/3/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "Annotaion.h"


@implementation Annotaion

@dynamic icon;
@dynamic lat;
@dynamic longt;
@dynamic name;
@dynamic photo;
@dynamic rating;
@dynamic reference;
@dynamic types;
@dynamic uniqueID;
@dynamic vicinity;
@dynamic type;

@end

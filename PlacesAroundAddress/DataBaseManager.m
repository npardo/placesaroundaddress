//
//  DataBaseManager.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "DataBaseManager.h"
#import "DownloadAccessor.h"
#import "Annotaion+Fetch.h"

NSString *const ManagedObjectReadyNotification = @"ManagedObjectReadyNotification";
NSString *const AnnotationsReadyNotification = @"AnnotationsReadyNotification";

static NSString *ResultsKey = @"results";

@implementation DataBaseManager

+ (DataBaseManager *)instance{
    static DataBaseManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (NSURL *)documentPath{
    NSURL *url = [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
                                                         inDomains:NSUserDomainMask] lastObject];
    return [url URLByAppendingPathComponent:@"DocumentForDB"];
}


- (void)initializeManagedObject{
    NSURL *url = [self documentPath];
    UIManagedDocument *document = [[UIManagedDocument alloc] initWithFileURL:url];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:url.path]) {
        [document saveToURL:url
           forSaveOperation:UIDocumentSaveForCreating
          completionHandler:^(BOOL success) {
              _managedContext = document.managedObjectContext;
              // First time the app is installed
              [self post];
          }];
    }else if (document.documentState == UIDocumentStateClosed){
        [document openWithCompletionHandler:^(BOOL success) {
            _managedContext = document.managedObjectContext;
            // App runs after deleted from the multitasking
            [self post];
        }];
    }else{
        
        // App came from the background
        _managedContext = document.managedObjectContext;
        [self post];
    }
}

- (void)post{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:ManagedObjectReadyNotification
                                                            object:nil];
    });
}

- (void)updateDataBase:(NSURL *)getRequest{
    DownloadAccessor *downloader = [DownloadAccessor new];
    [downloader startDownloadAtUrl:getRequest
               withCompletionBlock:^(NSData *data, long long dataSize, long long totalSize, NSError *error) {
                   if (data) {
                       dispatch_async(dispatch_get_main_queue(), ^{
                           [self insertAnnotations:data];
                       });
                   }else if (error){
                       NSLog(@"Download Error at DataBaseManager");
                   }
               }];
}

- (void)insertAnnotations:(NSData *)data{
    NSError *parsedError = nil;
    NSDictionary *parsedFile = [NSJSONSerialization JSONObjectWithData:data
                                                               options:kNilOptions
                                                                 error:&parsedError];
    NSArray *annotations = parsedFile[ResultsKey];
    __block NSUInteger counter = annotations.count;
    dispatch_queue_t fetchQ = dispatch_queue_create("FetchEvents", NULL);
    dispatch_async(fetchQ, ^{
        [_managedContext performBlock:^{
            for (NSDictionary *params in annotations) {
                counter--;
                [Annotaion annotationWithDictionary:params
                                    inManagedObject:_managedContext
                                           withType:_annotationType];
                if (!counter) {
                    [[NSNotificationCenter defaultCenter] postNotificationName:AnnotationsReadyNotification object:nil];
                }
            }
        }];
    });
}

- (NSArray *)annotationsFromDB{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Annotaion"];
    NSError *error = nil;
    NSArray *matches = [_managedContext executeFetchRequest:request error:&error];
    NSMutableArray *temp = [NSMutableArray new];
    for (Annotaion *ann in matches) {
        if ([ann.type isEqualToString:_annotationType]) {
            [temp addObject:ann];
        }
    }
    return [temp copy];
}

- (NSArray *)itemByEntity:(NSString *)entityName andAttribute:(NSString *)attribute andVal:(id)val{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%@ == %@", attribute, val];
    request.predicate = predicate;
    NSError *error = nil;
    NSArray *matches = [_managedContext executeFetchRequest:request error:&error];
    return matches;
}
@end

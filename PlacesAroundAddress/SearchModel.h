//
//  SearchModel.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 10/31/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DownloadAccessor.h"

extern NSString *const ResultsReadyNotification;
extern NSString *const DescriptionKey;

@interface SearchModel : NSObject{
    // Stores the results for reuse (for better performance in the look process)
    NSMutableDictionary *cachedResults;
    
    // The text for search becomes the key for use it later (when user deletes a letter the app
    // will present the previous result with out getting a request)
    NSString *textKey;
}


// See textKey above
- (NSArray *)typed:(NSString *)text;
- (void)cleanCache;

// Deals with the keyboard and the tableview sizes
- (CGRect)tableViewRect:(NSNotification *)keyboardNotification
                 tvRect:(CGRect)tvRect
                 isShow:(int)isShow;

@property (nonatomic, strong) NSArray *results;
@end

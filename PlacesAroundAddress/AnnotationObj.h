//
//  AnnotationObj.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>
#import "Annotaion.h"


@interface AnnotationObj : NSObject<MKAnnotation>{
    Annotaion *curAnnotation;
}
- (id)initWithAnnotation:(Annotaion *)annotation;

@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *uniqueID;
@end

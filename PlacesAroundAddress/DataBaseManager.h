//
//  DataBaseManager.h
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

extern NSString *const ManagedObjectReadyNotification;
extern NSString *const AnnotationsReadyNotification;

@interface DataBaseManager : NSObject{
    NSURL *linkToGoogleAPI;
}
    

+ (DataBaseManager *)instance;

// Starts the managed object
- (void)initializeManagedObject;

// Updates the data base according to new annotations
- (void)updateDataBase:(NSURL *)getRequest;

// Returns the annotations according to the type
- (NSArray *)annotationsFromDB;

// Not working I will check it later
- (NSArray *)itemByEntity:(NSString *)entityName
             andAttribute:(NSString *)attribute
                   andVal:(id)val;

// 
@property (nonatomic, strong) NSManagedObjectContext *managedContext;
@property (nonatomic, strong) NSString *annotationType;
@end

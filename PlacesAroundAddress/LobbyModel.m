//
//  LobbyModel.m
//  PlacesAroundAddress
//
//  Created by Nissim Pardo on 11/1/13.
//  Copyright (c) 2013 Nissim Pardo. All rights reserved.
//

#import "LobbyModel.h"
#import "LocatioManager.h"

static NSString *DescriptionKey = @"description";

@implementation LobbyModel
@synthesize types = _types;
@synthesize currentLocation = _currentLocation;
@synthesize coordinateForMap = _coordinateForMap;

- (id)init{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(reversedLocationReady:)
                                                     name:CoordinateReadyNotification
                                                   object:nil];
        return self;
    }
    return nil;
}

// Get coordinates for address (from the list)
- (void)coordinatesForAddress:(NSDictionary *)address{
    NSString *description = address[DescriptionKey];
    description = [description stringByReplacingOccurrencesOfString:@","
                                                         withString:@""];
    [[LocatioManager instance] coordinatesForAddress:description];
}

// On launch by default the app will get the current location, and if the user will choose it
- (void)getCurrentLocation{
    [[LocatioManager instance] startTrackingCurrentLocation];
}

// Convert placemark to One string
- (NSString *)addressFromPlaceMark:(CLPlacemark *)placemark{
    [self getCurrentLocation];
    NSArray *addressComp = placemark.addressDictionary[@"FormattedAddressLines"];
    NSString *address = [addressComp componentsJoinedByString:@" "];
    return address;
}

// Get the list of types from the plist
- (NSDictionary *)types{
    if (!_types) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Types" ofType:@"plist"];
        _types = [NSDictionary dictionaryWithContentsOfFile:path];
    }
    return _types;
}

// Transform the current loaction to string
- (NSString *)currentLocation{
    CLLocationCoordinate2D c2d = [LocatioManager instance].currentLocation;
    _currentLocation = [NSString stringWithFormat:@"%f,%f",c2d.latitude, c2d.longitude];
    return _currentLocation;
}

// When the call back arrives the result will be stored in reversedLocation
- (void)reversedLocationReady:(NSNotification *)notification{
    reversedLocation = notification.object;
}

// Give the coordinate for the map, check the correct one, between the current or the address (depends on the segment control)
- (NSString *)coordinateForMap{
    _coordinateForMap = _isCurrent ? _currentLocation : reversedLocation;
    return _coordinateForMap;
}
@end
